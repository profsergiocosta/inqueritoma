import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from navbar import Navbar

from dategraph import fig

import plotly.express as px

from service import dfEpic

df = dfEpic.groupby(['5_O_morador_deseja_p'])['5_O_morador_deseja_p'].count()
fig2 = px.bar(df, title="Quantidade de entrevistas")

fig2.update_layout(
    xaxis_title="Deseja Participar?",
    yaxis_title="Quantidade de Entrevista",
    legend_title="Legend Title",
    showlegend=False
)

nav = Navbar()

body = dbc.Container(
    [
       dbc.Row(
           [
               dbc.Col(
                  [
                    dcc.Graph(
                         figure= fig2
                     ),               
                  
                   ],
                  md=6,
               ),
              dbc.Col(

                  
                 [
                     
                     dcc.Graph(
                         figure= fig
                            ),
                  ]
                ),
              ]
            )
       ],
className="mt-4",
)

def Homepage():
    layout = html.Div([
    nav,
    body
    ])
    
    return layout

