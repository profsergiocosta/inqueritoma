import numpy as np
import pandas as pd
import dash
import dash_table
import dash_html_components as html
import dash_bootstrap_components as dbc
from navbar import Navbar

from service import dfEpic





df = dfEpic.groupby(['created_by','5_O_morador_deseja_p','4_Este_domiclio_foi_']).created_by.count()
#print (df)
df = df.unstack().reset_index()
#print (df)

nav = Navbar()

cols = [{"name": i, "id": i} for i in df.columns]
cols[0]["name"] = "Email"
cols[1]["name"] = "Deseja Participar?"

body =  dbc.Container(
    [
       dbc.Row(
           [
               dbc.Col([

                    dash_table.DataTable(
                        id='table',
                        columns=cols,
                        data=df.to_dict('records'),

                        style_data={ 'border': '3px solid #e5e5e5' },
                        
                        style_cell={'textAlign': 'center',
                        'padding': '8px',
                        'fontSize':14,  
                        },
                        
                        style_header={'fontWeight': 'bold',
                        'fontSize':16,
                        'border': '3px solid #e5e5e5',
                        },

                    )
               ])
           ])
    ]
)

def EmailReport():
    layout = html.Div([
    nav,
    body
    ])
    
    return layout

