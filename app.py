import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc

#from app import App, build_graph
from homepage import Homepage

from emailreport import EmailReport
from setorreport import SetorReport
from dategraph import DateGraph
from totalreport import TotalReport
from docreport import DocReport
from aglomeradoreport import AglomeradoReport
from nomereport import NomeReport

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.UNITED])

server = app.server

app.config.suppress_callback_exceptions = True

app.layout = html.Div([
    dcc.Location(id = 'url', refresh = False),
    html.Div(id = 'page-content')
])

@app.callback(Output('page-content', 'children'), [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/emailreport':
        return EmailReport()
    elif pathname == '/setorreport':
        return SetorReport()
    elif pathname == '/dategraph':
        return DateGraph()
    elif pathname == '/totalreport':
        return TotalReport()
    elif pathname == '/docreport':
        return DocReport()
    elif pathname == '/aglomeradoreport':
        return AglomeradoReport()
    elif pathname == '/nomereport':
        return NomeReport()
    else:
        return Homepage()


if __name__ == '__main__':
    app.run_server(debug=True)