import dash_bootstrap_components as dbc

def Navbar():
   navbar = dbc.NavbarSimple(
           children=[
              #dbc.NavItem(dbc.NavLink("Por Email", href="/emailreport")),
              dbc.DropdownMenu(
                 nav=True,
                 in_navbar=True,
                 label="Tabelas",
                 children=[
                    dbc.DropdownMenuItem(dbc.NavLink("Por Email", href="/emailreport")),
                    dbc.DropdownMenuItem(dbc.NavLink("Por Setor", href="/setorreport")),
                    dbc.DropdownMenuItem(dbc.NavLink("Por Aglomerado", href="/aglomeradoreport")),
                    dbc.DropdownMenuItem(dbc.NavLink("Por Documento", href="/docreport")),
                    dbc.DropdownMenuItem(dbc.NavLink("Total", href="/totalreport")),
                    #dbc.DropdownMenuItem(divider=True),
                    #dbc.DropdownMenuItem("Entry 3"),
                          ],
                      ),
                    ],
          brand="Home",
          brand_href="/home",
          sticky="top",
        )
   return navbar