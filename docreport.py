import numpy as np
import pandas as pd
import dash
import dash_table
import dash_html_components as html
import dash_bootstrap_components as dbc
from navbar import Navbar

from service import dfEpic



df = dfEpic.loc[ dfEpic["5_O_morador_deseja_p"] == "Deseja participar"]

df2 = df.groupby(['created_by','12_Qual_o_tipo_do_do','valida_doc']).created_by.count()
df2 = df2.unstack().reset_index().dropna()
df2 = df2.sort_values (by=["False"], ascending=False)


#df = dfEpic.groupby(['created_by','12_Qual_o_tipo_do_do','valida_doc']).created_by.count()
df = df.groupby(['12_Qual_o_tipo_do_do','valida_doc'])["12_Qual_o_tipo_do_do"].count()
df = df.unstack().reset_index().dropna()



nav = Navbar()

cols = [{"name": i, "id": i} for i in df.columns]
cols[0]["name"] = "Tipo do documento"
cols[1]["name"] = "Incorretos"
cols[2]["name"] = "Corretos"

cols2 = [{"name": i, "id": i} for i in df2.columns]
cols2[0]["name"] = "Email"
cols2[1]["name"] = "Tipo do documento"
cols2[2]["name"] = "Incorretos"
cols2[3]["name"] = "Corretos"


body =  dbc.Container(
    [
       dbc.Row(
           [
               dbc.Col([

                    dash_table.DataTable(
                        id='table',
                        columns=cols,
                        data=df.to_dict('records'),

                        style_data={ 'border': '3px solid #e5e5e5' },
                        
                        style_cell={'textAlign': 'center',
                        'padding': '8px',
                        'fontSize':14,  
                        },
                        
                        style_header={'fontWeight': 'bold',
                        'fontSize':16,
                        'border': '3px solid #e5e5e5',
                        },

                    )
               ])
           ]),

                  dbc.Row(
           [
               dbc.Col([

                    dash_table.DataTable(
                        id='table',
                        columns=cols2,
                        data=df2.to_dict('records'),

                        style_data={ 'border': '3px solid #e5e5e5' },
                        
                        style_cell={'textAlign': 'center',
                        'padding': '8px',
                        'fontSize':14,  
                        },
                        
                        style_header={'fontWeight': 'bold',
                        'fontSize':16,
                        'border': '3px solid #e5e5e5',
                        },

                    )
               ])
           ])
    ]
)

def DocReport():
    layout = html.Div([
    nav,
    body
    ])
    
    return layout

