import numpy as np
import pandas as pd
import dash
import dash_core_components as dcc
import dash_table
import dash_html_components as html
import dash_bootstrap_components as dbc
from navbar import Navbar

from service import dfEpic

import plotly.express as px



df = dfEpic.groupby(['date'])['date'].count()
fig = px.bar(df, title="Quantidade de entrevistas por dia")

fig.update_layout(
    xaxis_title="Data da Entrevista",
    yaxis_title="Quantidade de Entrevista",
    legend_title="Legend Title",
    showlegend=False
)

nav = Navbar()

body = dcc.Graph(
                id='example-graph-2',
                figure=fig
)

def DateGraph():
    layout = html.Div([
    nav,
    body
    ])
    
    return layout




