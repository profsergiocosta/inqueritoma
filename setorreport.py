import numpy as np
import pandas as pd
import dash
import dash_table
import dash_html_components as html
import dash_bootstrap_components as dbc
from navbar import Navbar

from service import dfEpic





df = dfEpic.groupby(['1_Informe_o_municpio','2_Informe_o_setor_ce','5_O_morador_deseja_p'])['2_Informe_o_setor_ce'].count()
df = df.unstack().reset_index()

df = df.sort_values (by=["Deseja participar"], ascending=False)

nav = Navbar()

cols = [{"name": i, "id": i} for i in df.columns]
cols[0]["name"] = "Município"
cols[1]["name"] = "Setor"

body =  dbc.Container(
    [
       dbc.Row(
           [
               dbc.Col([

                    dash_table.DataTable(
                        id='table',
                        columns=cols,
                        data=df.to_dict('records'),

                        style_cell={'textAlign': 'center',
                        'padding': '8px',
                        'fontSize':14,  
                        },
                        
                        style_header={'fontWeight': 'bold',
                        'fontSize':16,
                        'border': '3px solid #e5e5e5',
                        },
                    )
               ])
           ])
    ]
)

def SetorReport():
    layout = html.Div([
    nav,
    body
    ])
    
    return layout

