import numpy as np
import pandas as pd
import dash
import dash_table
import dash_html_components as html
import dash_bootstrap_components as dbc
from navbar import Navbar
import dateutil.parser

from service import get_all_entries


data = get_all_entries(300)[250:]
#print (data)
df = pd.DataFrame(data)


df = df.groupby(['created_at', '5_O_morador_deseja_p'])['created_at'].count()
#print (df)
df = df.unstack().reset_index()

#print (df['created_at'])

#for i in df['created_at']: print((dateutil.parser.parse(i).strftime('%m/%d/%Y'))) - retorna a data no formato dd/mm/yygit 
#to querendo agrupar por data no formato dd/mm/yy, porém o formato que estamos recebendo da
#epicollect parece que n é compatível. O pandas tem o dt que é pra trabalhar com datas

nav = Navbar()

body = dash_table.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in df.columns],
    data=df.to_dict('records'),
)


body =  dbc.Container(
    [
       dbc.Row(
           [
               dbc.Col([

                    dash_table.DataTable(
                        id='table',
                        columns=[{"name": i, "id": i} for i in df.columns],
                        data=df.to_dict('records'),
                    )
               ])
           ])
    ]
)

def DataReport():
    layout = html.Div([
    nav,
    body
    ])
    
    return layout

