import numpy as np
import pandas as pd
import dash
import dash_table
import dash_html_components as html
import dash_bootstrap_components as dbc
from navbar import Navbar

from service import dfEpic





df = dfEpic.loc[ dfEpic["5_O_morador_deseja_p"] == "Deseja participar"]

df = df.groupby(['contaNome']).ec5_uuid.count()


df = df.reset_index()

df = df.sort_values (by=["ec5_uuid"], ascending=False)

nav = Navbar()

cols = [{"name": i, "id": i} for i in df.columns]
#cols[0]["name"] = "Município"
#cols[2]["name"] = "Quantidade"
#cols[1]["name"] = "Agrupamento"
#

body =  dbc.Container(
    [
       dbc.Row(
           [
               dbc.Col([

                    dash_table.DataTable(
                        id='table',
                        columns=cols,
                        data=df.to_dict('records'),

                        style_table={'overflowX': 'auto'},

                        style_cell={'textAlign': 'center',
                         'whiteSpace': 'normal',
                        'padding': '8px',
                        'height': 'auto',
                        'minWidth': '30%', 'width': '30%', 'maxWidth': '30%',
                        'fontSize':14,  
                        },
                        
                        style_header={'fontWeight': 'bold',
                        'fontSize':16,
                        'border': '3px solid #e5e5e5',
                        },
                    )
               ])
           ])
    ]
)

def NomeReport():
    layout = html.Div([
    nav,
    body
    ])
    
    return layout

